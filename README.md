# COBRA_TS experiments

This git contains scripts used to produce and analyze results for the COBRAS time series paper.
Going through this README and following all the instructions should result in a folder with result plots being generated.

## Configuration files

There is one general configuration file: configs/main.
Before running anything, you should modify all paths there.

There are also separate configuration files per clustering method.
They are currently configured to only run for two UCR datasets ('ECG200' and 'CBF').
Configuring them to run on all of them is easy: simply add the names to the 'fns' setting in the configuration files.

## Generating folds

From code execute `python generate_folds.py`

The datasets for which folds will generated are set at the start of the script.

## Running COBRAS and competitors

### Generating distance matrices

Both COBRAS_DTW and COBS require the DTW distance matrix. The following script computes all the distance matrices once,
so that they can be re-used for different algorithms and folds.

From code execute `python run_experiments.py compute_distance_matrices.cfg`

### Running COBRAS_DTW

Once distance matrices have been generated, COBRAS_DTW can be executed as follows

From code execute `python run_experiments.py cobras_dtw.cfg`

### Running COBRAS_kShape

From code execute `python run_experiments.py cobras_kshape.cfg`

### Running kShape

From code execute `python run_experiments.py kshape.cfg`

### Running COBS

This one requires an extra command, as we first generate the grid of unsupervised clusterings and then re-use it for
different folds.

First generate a worker file containing the desired algorithm and parameter settings:

From code execute `python generate_cobs_grid_worker_file.py`

Next, execute the unsupervised algorithms for the specified parameter settings:

From code execute `python run_experiments.py cobs_grid.cfg`

Finally, run COBS on the generated grid:

From code execute `python run_experiments.py cobs.cfg`

## Computing the ARIs

`python run_experiments.py compute_aris_kshape.cfg`

`python run_experiments.py compute_aris_cobs.cfg`

`python run_experiments.py compute_aris_cobras_kshape.cfg`

`python run_experiments.py compute_aris_cobras_dtw.cfg`

## Plotting the results for a specific dataset

`python plot_dataset_results.py ECG200`

## Plotting aggregated results

The datasets to use are set at the top of the scripts

`python plot_aligned_rank.py`

`python plot_average_ari.py`

## Comparing cDTWSS to kShape

To do this, first generate all the kShape clusterings and compute their Rand index scores:

From code execute `python run_experiments.py kshape.cfg`
From code execute `python run_experiments.py comput_ris_kshape.cfg`

Next, execute the following to print the kShape and cDTWss RIs to the output in CSV format:

From code execute `python compare_kshape_to_cdtwss.py`

This uses the file code/cdtwss_scores32, which contains the cDTWss scores given 32 constraints.
These scores are obtained from https://sites.google.com/site/dtwclustering/

We also provide the pre-computed scores in code/kshape_vs_cdtwss_ris.csv

## Datasets that are left out of the ARI computations

### cDTWss timed out for the following datasets:
ElectricDevices, FordB, HandOutlines, Phoneme, NonInvasiveFatalECG\_Thorax1, NonInvasiveFatalECG\_Thorax2, StarLightCurves, UWaveGestureLibraryAll, MALLAT, Two\_Patterns

### cDTWss crashed for the following datasets:
FordA, ShapeletSim, Meat, Cricket\_Z, Earthquakes, SmallKitchenAppliances, wafer, Coffee, OliveOil, Wine, BeetleFly




