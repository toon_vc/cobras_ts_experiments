import os
import sys
import numpy as np
import time
import configparser

from dtaidistance import dtw

fn = sys.argv[1]
window = int(sys.argv[2])

config = configparser.ConfigParser()
config.read('../configs/main')

ucr = config['main']['ucr_path']
distances_path = config['main']['distance_matrices_path']

data = np.loadtxt(os.path.join(ucr,fn,fn + '_TEST'), delimiter=',')
series = data[:,1:]

if os.path.exists(os.path.join(distances_path, fn, str(window))):
    print("This distance matrix has already been computed, delete if you want to re-compute.")
    exit()

if not os.path.exists(os.path.join(distances_path,fn)):
    os.makedirs(os.path.join(distances_path,fn))

start = time.time()
dists = dtw.distance_matrix(series, window=int(0.01 * window * series.shape[1]))
np.savetxt(os.path.join(distances_path,fn,str(window)), dists)

with open(os.path.join(os.path.join(distances_path,fn,str(window) + ".runtime")), 'w+') as f:
  f.write(str(time.time() - start))
