import os
import sys
import numpy as np
import json
import configparser
from sklearn import metrics

dataset = sys.argv[1]
fold = int(sys.argv[2])
budget = int(sys.argv[3])
alpha = float(sys.argv[4])
w = int(sys.argv[5])

config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = os.path.join(config['main']['clusterings_path'],'cobras_dtw',dataset)
scores_path = os.path.join(config['main']['scores_path'],'cobras_dtw',dataset)
folds_path = os.path.join(config['main']['folds_path'],dataset)


if os.path.exists(scores_path + "/" + "_".join(sys.argv[2:]) + ".aris"):
    exit()

test_idx = np.load(os.path.join(folds_path, str(fold) + ".test.npy")).tolist()

data = np.loadtxt(os.path.join(config['main']['ucr_path'], dataset, dataset + '_TEST'), delimiter=',')
labels = data[:, 0]

labels_to_evaluate = [x for i, x in enumerate(labels) if i in test_idx]

if not os.path.exists(scores_path):
    os.makedirs(scores_path)

with open(clusterings_path + "/" + "_".join(sys.argv[2:])) as result_file:
    result_data = json.load(result_file)
    aris = np.zeros((budget,))
    for idx in range(min(budget, len(result_data['clusterings']))):
        to_evaluate = [x for i, x in enumerate(result_data['clusterings'][idx]) if i in test_idx]
        aris[idx] = metrics.adjusted_rand_score(labels_to_evaluate, to_evaluate)

    for idx in range(len(result_data['clusterings']), budget):
        aris[idx] = aris[idx - 1]

    np.save(scores_path + "/" + str(fold) + "_" + str(budget) + ".aris", aris)