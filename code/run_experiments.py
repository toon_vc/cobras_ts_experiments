import configparser
import ast
import itertools
import os
import time
import random
import sys


def chunkify(lst,n):
    return [lst[i::n] for i in range(n)]

def divide_worker_input(config, experiments_path):
    main_config = configparser.ConfigParser()
    main_config.read('../configs/main')

    hosts = ast.literal_eval(config['hosts']['names'])
    worker_file = open(os.path.join(main_config['main']['tmp_path'],config['main']['worker_file']))
    header = worker_file.readline().rstrip().split(',')
    lines = list(map(lambda line: line.rstrip().split(','), worker_file.readlines()))
    
    random.shuffle(lines)
    
    jobs_per_host = chunkify(lines, len(hosts))

    for idx, host in enumerate(hosts):
        f = open(os.path.join(experiments_path, 'worker_input_' + str(host)), 'w+')
        f.write(",".join(header) + '\n')
        for i in jobs_per_host[idx]:
            f.write(",".join(i) + '\n')
        f.close()

    return header

def write_worker_input(config, experiments_path):
    hosts = ast.literal_eval(config['hosts']['names'])

    param_and_value = dict()
    for k in config['parameters']:
        value_list = ast.literal_eval(config['parameters'][k])
        param_and_value[k] = [(k, v) for v in value_list]

    instantiations = list(itertools.product(*[v for v in param_and_value.values()]))

    random.shuffle(instantiations)
    
    jobs_per_host = chunkify(instantiations,len(hosts))

    for idx, host in enumerate(hosts):
        f = open(os.path.join(experiments_path, 'worker_input_' + str(host)), 'w+')
        f.write(",".join(config['parameters']) + '\n')
        for i in jobs_per_host[idx]:
            to_write = []
            for k in config['parameters']:
                to_write.append([x[1] for x in i if x[0] == k][0])
            f.write(",".join([str(x) for x in to_write]) + '\n')
        f.close()

    return config['parameters']

def write_bash_script(config, params):
    cur_path = os.path.dirname(os.path.abspath(__file__))
    f = open(os.path.join(experiments_path,'bash.pbs'),'w+')
    f.write('# !/bin/bash -l\n')
    f.write('cd ' + cur_path + '\n')
    f.write('python ' + config['main']['script'] + ' ' + ' '.join(['$' + x for x in params]))
    f.close()

def start_jobs(config, experiments_path):
    main_config = configparser.ConfigParser()
    main_config.read('../configs/main')

    bash = os.path.join(experiments_path,'bash.pbs')
    script_path = os.path.dirname(os.path.abspath(__file__))
    logs_path = os.path.join(experiments_path,'logs')

    hosts = ast.literal_eval(config['hosts']['names'])
    for idx, host in enumerate(hosts):
        #print("ssh " + host + " 'cd " + script_path + "; nohup python host_runner.py " + bash + " " + experiments_path + "/worker_input_" + host + " " + str(int(config['hosts']['nbcores'])) + " &> " + os.path.join(logs_path,host) + " &'")
        bla = os.system("ssh " + host + " 'cd " + script_path + "; source " + main_config['main']['venv_path'] + "; export PYTHONPATH=" + main_config['main']['cobras_ts_path'] + ":$PYTHONPATH;nohup python host_runner.py " + bash + " " + experiments_path + "/worker_input_" + host + " " + str(int(config['hosts']['nbcores'])) + " &> " + os.path.join(logs_path,host) + " &'")


main_config = configparser.ConfigParser()
main_config.read('../configs/main')

config = configparser.ConfigParser()
config.read(os.path.join(main_config['main']['cfg_path'],sys.argv[1]))

timestamp = int(time.time())
experiments_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),main_config['main']['tmp_path'], str(timestamp))
os.makedirs(experiments_path)
os.mkdir(os.path.join(experiments_path,'logs'))

if 'worker_file' in config['main']:
    params = divide_worker_input(config, experiments_path)
else:
    params = write_worker_input(config, experiments_path)

write_bash_script(config, params)
start_jobs(config, experiments_path)
