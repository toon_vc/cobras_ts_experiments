import sys
import os
import configparser


config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = config['main']['clusterings_path']
folds_path = config['main']['folds_path'] + '/'
ucr_path = config['main']['ucr_path'] + '/'
result_path = config['main']['clusterings_path'] + '/cdtwss/'

if not os.path.exists(result_path):
    os.makedirs(result_path)

dataset = sys.argv[1]
fold = sys.argv[2]

if not os.path.exists(os.path.join(clusterings_path, 'cdtwss' , dataset + '.' + fold)):
    os.system("matlab -nodesktop -nosplash -r \"generate_clusterings({'" + dataset + "'},'" + fold + "','" + folds_path + "','" + ucr_path + "','" + result_path + "'); exit;\"")
