import numpy as np
import os
import json
import configparser
import sys
from cobras_ts import cobras_dtw

dataset = sys.argv[1]
fold = int(sys.argv[2])
budget = int(sys.argv[3])
alpha = float(sys.argv[4])
w = int(sys.argv[5])

config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = os.path.join(config['main']['clusterings_path'],'cobras_dtw',dataset)
folds_path = config['main']['folds_path']
dists_path = os.path.join(config['main']['distance_matrices_path'],dataset)

data = np.loadtxt(os.path.join(config['main']['ucr_path'],dataset,dataset + '_TEST'), delimiter=',')
series = data[:,1:]
labels = data[:,0]

if not os.path.exists(os.path.join(clusterings_path)):
    os.makedirs(os.path.join(clusterings_path))

dists = np.loadtxt(os.path.join(dists_path,str(w)))
dists[dists == np.inf] = 0
dists = dists + dists.T - np.diag(np.diag(dists))
affinities = np.exp(-dists * alpha)

train_idx = np.load(os.path.join(folds_path,dataset,str(fold) + ".train.npy")).tolist()

clusterer = cobras_dtw.COBRAS_DTW(affinities, labels, budget, train_idx)
clusterings, runtimes, ml, cl = clusterer.cluster()

#print(clusterings)


f = open(clusterings_path + "/" + "_".join(sys.argv[2:]), 'w+')
f.write(json.dumps({'clusterings' : clusterings, 'runtimes' : runtimes, 'ml' : ml, 'cl' : cl}))
f.close()
