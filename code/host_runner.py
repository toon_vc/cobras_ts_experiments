import sys, os,subprocess
from multiprocessing import Pool

bash = sys.argv[1]
data = open(sys.argv[2])
nbCores = int(sys.argv[3])

header = data.readline().rstrip().split(',')

lines = map(lambda line: line.rstrip().split(','), data.readlines())


def executeLine(line):
  env=os.environ.copy()
  for k,v in zip(header,line):
    env[k]=v
  subprocess.call(['bash',bash], env=env)


p = Pool(nbCores)
p.map(executeLine, lines)