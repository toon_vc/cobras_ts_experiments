import os
import sys
import numpy as np
import json
import time
import configparser
from kshape.core import kshape

dataset = sys.argv[1]

main_config = configparser.ConfigParser()
main_config.read('../configs/main')

ucr_dir = main_config['main']['ucr_path']
output_path = os.path.join(main_config['main']['clusterings_path'],'kshape',dataset)
dists_path = os.path.join(main_config['main']['distance_matrices_path'],dataset)

data = np.loadtxt(os.path.join(main_config['main']['ucr_path'],dataset,dataset + '_TEST'), delimiter=',')
labels = data[:,0]
series = data[:,1:]

if os.path.exists(output_path):
    exit()

if not os.path.exists(os.path.join(main_config['main']['clusterings_path'],'kshape')):
    os.makedirs(os.path.join(main_config['main']['clusterings_path'],'kshape'))

start = time.time()

clusters = kshape(series, len(set(labels)))

cluster_labels = [-1]*len(labels)
for c_idx, (centroid,idxs) in enumerate(clusters):
    for i in idxs:
        cluster_labels[i] = c_idx


runtime = time.time() - start

f = open(output_path, 'w+')
f.write(json.dumps({'clustering' : cluster_labels, 'runtime' : runtime}))
f.close()
