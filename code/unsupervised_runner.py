import os
import sys
import numpy as np
import json
import time
import configparser
from sklearn import cluster, metrics

dataset = sys.argv[1]
algo = sys.argv[2]

try:
    gamma = float(sys.argv[3])
except:
    gamma = 0

try:
    n_clusters = int(sys.argv[4])
except:
    pass

linkage = sys.argv[5]

try:
    damping = float(sys.argv[6])
except:
    pass

try:
    epsilon = float(sys.argv[7])
except:
    pass

try:
    minPts = int(sys.argv[8])
except:
    pass

window_size = int(sys.argv[9])

main_config = configparser.ConfigParser()
main_config.read('../configs/main')

ucr_dir = main_config['main']['ucr_path']
output_path = os.path.join(main_config['main']['clusterings_path'],'cobs',dataset,'grid')
dists_path = os.path.join(main_config['main']['distance_matrices_path'],dataset)

if not os.path.exists(output_path):
    os.makedirs(output_path)

start = time.time()

if os.path.exists(output_path + "/" + "_".join(sys.argv[2:])):
    print("here?")
    exit()



dists = np.loadtxt(os.path.join(dists_path,str(window_size)))
dists[dists == np.inf] = 0
dists = dists + dists.T - np.diag(np.diag(dists))
affinities = np.exp(-dists * gamma)

if algo == 'hierarchical':
    clusterer = cluster.AgglomerativeClustering(n_clusters=n_clusters,affinity='precomputed',linkage=linkage)
    predicted_y = clusterer.fit_predict(affinities).tolist()
elif algo == 'spectral':
    clusterer = cluster.SpectralClustering(n_clusters=n_clusters,affinity='precomputed')
    predicted_y = clusterer.fit_predict(affinities).tolist()
elif algo == 'affinitypropagation':
    clusterer = cluster.AffinityPropagation(damping=damping)
    predicted_y = clusterer.fit_predict(affinities).tolist()
elif algo =='dbscan':
    np.fill_diagonal(dists,0.0)
    clusterer = cluster.DBSCAN(eps=epsilon,min_samples=minPts)
    predicted_y = clusterer.fit_predict(dists).tolist()

runtime = time.time() - start

if not os.path.exists(output_path):
    os.makedirs(output_path)

print("writing")
f = open(output_path + "/" + "_".join(sys.argv[2:]), 'w+')
f.write(json.dumps({'clustering' : predicted_y, 'runtime' : runtime}))
f.close()