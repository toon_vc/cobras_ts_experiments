import os
import numpy as np
import configparser
import matplotlib.pyplot as plt
import sys

dataset = sys.argv[1]

sample_size = 200
exchange_factor = 2.0
budget = 100
alpha = 0.5
w = 10

config = configparser.ConfigParser()
config.read('../configs/main')

n_folds = int(config['main']['n_folds'])
clusterings_path = os.path.join(config['main']['clusterings_path'],'kshape',dataset)
scores_path = os.path.join(config['main']['scores_path'],'kshape',dataset)

cobs_path = os.path.join(config['main']['scores_path'],'cobs',dataset)
cobras_dtw_path = os.path.join(config['main']['scores_path'],'cobras_dtw',dataset)
cobras_kshape_path = os.path.join(config['main']['scores_path'],'cobras_kshape',dataset)
kshape_path = os.path.join(config['main']['scores_path'],'kshape',dataset)

plots_path = os.path.join(config['main']['plots_path'])

if not os.path.exists(plots_path):
    os.makedirs(plots_path)

folds_path = os.path.join(config['main']['folds_path'],dataset)

idx_to_plot = range(10,110,10)
idx_to_plot = [x-1 for x in idx_to_plot]

data = np.loadtxt(os.path.join(config['main']['ucr_path'], dataset, dataset + '_TEST'), delimiter=',')
series = data[:, 1:]
labels = data[:, 0]

cobs_aris = np.zeros((n_folds, budget))
ris = np.zeros((n_folds,budget))

cdtwss_aris = np.zeros((n_folds, budget))
cobras_dtw_aris = np.zeros((n_folds, budget))
cobras_kshape_aris = np.zeros((n_folds, budget))
kshape_aris = np.zeros((n_folds, budget))
for fold in range(n_folds):
    cobs_aris[fold, :] = np.load(os.path.join(cobs_path, "_".join([str(x) for x in [fold, budget, exchange_factor, sample_size, w, alpha]]) + ".aris.npy")).tolist()
    cobras_dtw_aris[fold, :] = np.load(os.path.join(cobras_dtw_path, "_".join([str(x) for x in [fold, budget]]) + ".aris.npy")).tolist()
    cobras_kshape_aris[fold,:] = np.load(os.path.join(cobras_kshape_path,"_".join([str(x) for x in [fold,budget]]) + ".aris.npy")).tolist()
    #cdtwss_aris[fold, :] = np.load(cdtwss_path + "." + str(fold) + '.npy').tolist()
    kshape_aris[fold, :] = np.load(os.path.join(kshape_path,"_".join([str(x) for x in [fold]]) + ".aris.npy")).tolist()


plt.figure()
plt.title(dataset + "  " + str(len(labels)))
plt.plot(np.mean(cobs_aris, axis=0), label='COBS')
plt.plot(np.mean(cobras_dtw_aris, axis=0), label='COBRAS')
plt.plot(np.mean(cobras_kshape_aris,axis=0),label='COBRAS-kShape')
plt.plot(np.mean(kshape_aris, axis=0), label='kShape')
#plt.plot(idx_to_plot,np.mean(cdtwss_aris,axis=0)[idx_to_plot], label='cDTWss')

plt.legend(loc='best')
plt.ylim([0,1])
plt.xlim([0,100])
plt.savefig(os.path.join(plots_path,dataset + '.pdf'),bbox_inches='tight')

