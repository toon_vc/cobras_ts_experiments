import os
import numpy as np
import configparser
import matplotlib.pyplot as plt
import collections

def aligned_ranks(scores):
    '''
    Scores maps number of constraints to dataset to method to score
    '''

    differences_to_mean = collections.defaultdict(list)
    for num_constraints in scores:
        for dataset in datasets:
            mean_score = np.mean(list(scores[num_constraints][dataset].values()))

            for algo in scores[num_constraints][dataset]:
                differences_to_mean[num_constraints].append((scores[num_constraints][dataset][algo] - mean_score,algo,dataset))


    ranks = collections.defaultdict(list)

    for nc in scores:
        sorted_diff = sorted(differences_to_mean[nc], key=lambda x: x[0], reverse=True)

        cur_aligned_ranks = collections.defaultdict(int)

        for diff_idx, (cur_diff, method, dataset) in enumerate(sorted_diff):
            cur_aligned_ranks[method] += diff_idx + 1

        for method in cur_aligned_ranks:
            cur_aligned_ranks[method] /= float(len(datasets))

        for method in cur_aligned_ranks:
            ranks[method].append(cur_aligned_ranks[method] / len(datasets))

    return ranks

datasets = ['ECG200','Trace']


idx_to_plot = range(10,110,10)
idx_to_plot = [x-1 for x in idx_to_plot]



sample_size = 200
exchange_factor = 2.0
budget = 100
alpha = 0.5
w = 10

config = configparser.ConfigParser()
config.read('../configs/main')

n_folds = int(config['main']['n_folds'])
plots_path = os.path.join(config['main']['plots_path'])

if not os.path.exists(plots_path):
    os.makedirs(plots_path)

all_aris = np.zeros((len(datasets),budget))
all_cdtwss_aris = np.zeros((len(datasets),budget))
all_cobras_aris = np.zeros((len(datasets),budget))
all_cobras_kshape_aris = np.zeros((len(datasets),budget))
all_kshape_aris = np.zeros((len(datasets),budget))

scores = collections.defaultdict(lambda : collections.defaultdict(lambda: collections.defaultdict(float)))

for idx, dataset in enumerate(datasets):
    clusterings_path = os.path.join(config['main']['clusterings_path'], 'kshape', dataset)
    scores_path = os.path.join(config['main']['scores_path'], 'kshape', dataset)

    cobs_path = os.path.join(config['main']['scores_path'], 'cobs', dataset)
    cobras_dtw_path = os.path.join(config['main']['scores_path'], 'cobras_dtw', dataset)
    cobras_kshape_path = os.path.join(config['main']['scores_path'], 'cobras_kshape', dataset)
    kshape_path = os.path.join(config['main']['scores_path'], 'kshape', dataset)

    aris = np.zeros((n_folds,budget))

    cdtwss_aris = np.zeros((n_folds, budget))
    cobras_dtw_aris = np.zeros((n_folds, budget))
    cobras_kshape_aris = np.zeros((n_folds, budget))
    kshape_aris = np.zeros((n_folds, budget))

    for fold in range(n_folds):
        if dataset in ['BirdChicken','Beef']:
            # BirdChicken and Beef have too few points to sample 200 possible constraints for COBS, sample size is 100 there
            aris[fold,:] = np.load(os.path.join(cobs_path,"_".join([str(x) for x in [fold,100,exchange_factor,sample_size,w,alpha]]) + ".aris.npy")).tolist()
        else:
            aris[fold,:] = np.load(os.path.join(cobs_path,"_".join([str(x) for x in [fold,budget,exchange_factor,sample_size,w,alpha]]) + ".aris.npy")).tolist()

        cobras_dtw_aris[fold, :] = np.load(os.path.join(cobras_dtw_path, "_".join([str(x) for x in [fold, budget]]) + ".aris.npy")).tolist()
        cobras_kshape_aris[fold,:] = np.load(os.path.join(cobras_kshape_path,"_".join([str(x) for x in [fold,budget]]) + ".aris.npy")).tolist()
        kshape_aris[fold, :] = np.load(os.path.join(kshape_path, "_".join([str(x) for x in [fold]]) + ".aris.npy")).tolist()

    all_aris[idx,:] = np.mean(aris,axis=0)
    all_cdtwss_aris[idx,:] = np.mean(cdtwss_aris, axis=0)
    all_cobras_aris[idx,:] = np.mean(cobras_dtw_aris, axis=0)
    all_cobras_kshape_aris[idx,:] = np.mean(cobras_kshape_aris, axis=0)
    all_kshape_aris[idx,:] = np.mean(kshape_aris, axis=0)

    for i in range(100):
        scores[i][dataset]['COBS'] = all_aris[idx,i-1]
        scores[i][dataset]['COBRAS$^{DTW}$'] = all_cobras_aris[idx,i-1]
        scores[i][dataset]['cDTW$^{SS}$'] = all_cdtwss_aris[idx,i]
        scores[i][dataset]['COBRAS$^{kShape}$'] = all_cobras_kshape_aris[idx,i-1]
        scores[i][dataset]['kShape'] = all_kshape_aris[idx,i-1]


font = {'family': 'serif',
        'weight': 'normal',
        'size': 25,
        }

algo_font = {'family': 'serif',
        'weight': 'normal',
        'size': 25,
        }

ticks_font = {'family': 'serif',
             'weight': 'normal',
             'size': 18,
             }

ranks = aligned_ranks(scores)

algo_to_color = dict()
algo_to_color['cDTWss'] = 'red'
algo_to_color['COBRAS'] = 'green'
algo_to_color['COBS'] = 'blue'
algo_to_color['kShape'] = 'purple'


plt.figure()
for algo in ranks:
    print(algo)
    t = plt.plot(idx_to_plot,[ranks[algo][idx] for idx in idx_to_plot],label=algo,linewidth=4)
    plt.text(100, ranks[algo][idx_to_plot[-1]], algo, color=t[0].get_color(),fontdict=algo_font)

ax = plt.gca()
# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

plt.xticks(range(10,110,10),range(10,110,10))
ax.set_xticklabels(range(10,110,10),fontdict=ticks_font)

plt.yticks([ax.get_yticks()[0],ax.get_yticks()[-1]],[ax.get_yticks()[0],ax.get_yticks()[-1]])
ax.set_yticklabels([ax.get_yticklabels()[0],ax.get_yticklabels()[-1]],fontdict=ticks_font)

plt.xlim([10,100])
plt.figtext(0.1, 0.92, "Aligned rank", fontdict=font)
plt.figtext(0.7, -0.04, "Number of queries", fontdict=font)
ax.yaxis.set_label_coords(-10,1.02)

plt.savefig(os.path.join(plots_path,'aligned_rank.pdf'),bbox_inches='tight')
