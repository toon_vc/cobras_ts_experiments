import numpy as np
import configparser
import os

config = configparser.ConfigParser()
config.read('../configs/main')

datasets = ['ECG200','Trace']

f = open(os.path.join(config['main']['tmp_path'],'cobs_worker_input'), 'w+')
f.write("dataset,algo,gamma,n_clusters,linkage,damping,epsilon,minPts,windowsize\n")

none = "none"
cur_path = os.path.dirname(os.path.abspath(__file__))

ucr_path = config['main']['ucr_path']


for dataset in datasets:

    output_path = os.path.join(config['main']['clusterings_path'], 'cobs_grid_clusterings', dataset)
    dists_path = os.path.join(config['main']['distance_matrices_path'], dataset)

    file_path = os.path.join(ucr_path, dataset, dataset + "_TEST")
    data = np.loadtxt(file_path, delimiter=',')
    y = data[:, 0]

    true_k = len(set(y))
    min_k = max([2, true_k - 5])
    max_k = true_k + 5

    for windowsize in [str(x) for x in [10]]:
        for n_clusters in range(min_k, max_k + 1):
            #for gamma in np.arange(0.05, 2.0, 0.1):
            for gamma in np.arange(0.05,2.0,0.5):
                # NOTE: much coarser than in the paper, just to quickly test the running of this experimental pipeline
                if not os.path.exists(output_path + "/" + "_".join(
                        ["spectral", str(gamma), str(n_clusters), none, none, none, none, windowsize])):
                    f.write(",".join(
                        [dataset, "spectral", str(gamma), str(n_clusters), none, none, none, none, windowsize]) + "\n")
                for linkage in ["average", "complete"]:
                    if not os.path.exists(output_path + "/" + "_".join(
                            ["hierarchical", str(gamma), str(n_clusters), linkage, none, none, none, windowsize])):
                        f.write(",".join(
                            [dataset, "hierarchical", str(gamma), str(n_clusters), linkage, none, none, none,
                             windowsize]) + "\n")

    for windowsize in [str(x) for x in [10]]:
        #for gamma in np.arange(0.05, 2.0, 0.1):
        for gamm in np.arange(0.05,2.0,0.5):
            # NOTE: much coarser than in the paper, just to quickly test the running of this experimental pipeline
            #for damping in np.arange(0.5, 1.0, 0.025):
            for damping in np.arange(0.5,1.0,0.2):
                # NOTE: much coarser than in the paper, just to quickly test the running of this experimental pipeline
                if not os.path.exists(output_path + "/" + "_".join(
                        ["affinitypropagation", str(gamma), none, none, str(damping), none, none, windowsize])):
                    f.write(",".join([dataset, "affinitypropagation", str(gamma), none, none, str(damping), none, none,
                                      windowsize]) + "\n")

    dists = np.loadtxt(os.path.join(dists_path, windowsize))
    dists[dists == np.inf] = 0
    dists = dists + dists.T - np.diag(np.diag(dists))
    np.fill_diagonal(dists, 0.0)

    #min_samples = range(2, 22, 1)
    #eps = np.linspace(dists.min(), dists.max(), 20)
    min_samples = range(2, 22, 5)
    eps = np.linspace(dists.min(), dists.max(), 3)
    # NOTE: much coarser than in the paper, just to quickly test the running of this experimental pipeline


    for windowsize in [str(x) for x in [10]]:
        for e in eps:
            for ms in min_samples:
                if not os.path.exists(output_path + "/" + "_".join(
                        ["dbscan", none, none, none, none, str(e), str(ms), windowsize])):
                    f.write(",".join([dataset, "dbscan", none, none, none, none, str(e), str(ms), windowsize]) + "\n")

f.close()