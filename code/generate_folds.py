import random
import configparser
import os
import numpy as np
import ast
from sklearn import cross_validation

config = configparser.ConfigParser()
config.read('../configs/main')

ucr = config['main']['ucr_path']
folds_path = config['main']['folds_path']
n_folds = int(config['main']['n_folds'])
random.seed(int(config['main']['rseed']))

fns = ['ECG200','Trace']

for fn in fns:

    if os.path.exists(os.path.join(folds_path,fn)):
        print("Folds have already been generated for " + fn + ". Delete them first if you really want new ones.")
        continue

    os.makedirs(os.path.join(folds_path,fn))

    data = np.loadtxt(os.path.join(ucr,fn,fn + '_TEST'), delimiter=',')
    labels = data[:, 0]

    skf = cross_validation.StratifiedKFold(labels, n_folds=n_folds, shuffle=True)
    for fold_id, (train_index, test_index) in enumerate(skf):
        np.save(os.path.join(folds_path,fn,str(fold_id) + ".train"),train_index)
        np.save(os.path.join(folds_path,fn,str(fold_id) + ".test"),test_index)
