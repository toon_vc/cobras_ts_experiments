% Semi-supervised prediction of the best warping window width (w) for time
% series clustering under constrained DTW
%
% Calling syntax
% learned_w = main_clustering(dataset, cutoff_distance, max_w)
%
% Example of usage
% learned_w = main_clustering(data, 5, 20)
%
% Input
% data: the dataset (matrix). Each row is a time series. First column is
% label.
% cutoff_distance: The cutoff distance of TADPole clustering algorithm
% (scalar). 
% max_w: The range of w to be considered, from 0-max_W (integer)
%
%
% This function predicts w using utmost 16 constraints (annotation). This
% parameter can be configured.
% Upon finishing, it will display the result plot. The top curve of this
% plot is Rand-Index (groundtruth). The next curves is prediction vector
% using 1, 2, 4, 8, and 16 constraints respectively. The prediction vector
% does not reflect the true Rand-Index, but rather the expected change of
% clustering quality (for the better or worse) at each WW. 
%
%
% Note on the clustering algorithm used
% TADpole requires setting two parameters: number of clusters and cutoff
% distance. There are heuristics to set these parameter. For detailed
% discussion, please refer to the original paper. The project webpage for
% this paper is at http://www.cs.ucr.edu/~nbegu001/SpeededClusteringDTW/
% In this experiment, we set number of cluster to be the true number of
% clusters (groundtruth). For cutoff distance, most of the time we pick a 
% fixed cutoff distance for all WW. For some datasets, you may not be able
% to do so. You may have to use different cutoff distances for small WW and
% bigger WW.
%
%
% If you want to use another clustering algorithm, you can do so by replace
% TADpole in function filter_constraints with your favourite
% clustering algorithm using DTW as distance measure. The goal is to get
% the cluster assignment by that clustering algorithm for each WW.
% Subsequent steps are the same.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function [clusterings] = main_clustering(dataset, cutoff_distance, max_w, train_idxs)

    if ~exist('cutoff_distance', 'var')
        cutoff_distance = 5;
    end
    if ~exist('max_w', 'var')
        max_w = 20;  % w = 0-20%
    end
    
    disp(['Cutoff distance is ', num2str(cutoff_distance)]);
    disp(['Consider w from 0 to ', num2str(max_w)]);

    % data is a matrix with each row being one time series object
    % label is the row vector of groundtruth class assignment 
    data = dataset(:, 2:end);
    label = dataset(:, 1);
    
    N =  length(unique(label)); %number of clusters
    %label cannot be negative
    if min(label) < 0 || min(label) == 0
        old_min = min(label);
        new_min = 1;
        label = label + (new_min - old_min);
    end
    
 
    
    %find constraints that change label at least once with different WW
    disp('Finding unique volatile constraints');
    [volatile_constraints, cluster_assignment] = filter_constraints(data, N, cutoff_distance, max_w,train_idxs);
    disp('Done with finding volatile constraints');

    %sort the constraints based on their simplicity score, simplest first
    disp('Working on simplicity sort');
    sorted_constraints = simplicity_sort(volatile_constraints, max_w);
    disp('Done with simplicity sort');
    
    %plot Rand-Index groundtruth and prediction vectors using 1, 2, 4, 8,
    %16 constraints
    for i = 1:size(cluster_assignment, 1)
        RI = RandIndex(cluster_assignment(i,:),label);
        scores = zeros(16,1);
        scores(1) = RI;
        for j = 1:10
            evaluate_constraint(cluster_assignment(i,:), label, sorted_constraints, 10*j);
        end
        %scores(2) = evaluate_constraint(cluster_assignment(i,:), label, sorted_constraints, 16);
        %scores(3) = evaluate_constraint(cluster_assignment(i,:), label, sorted_constraints, 32);
        R(i,:) = scores;
    end
    
    clusterings = zeros(size(label,1),10);
    for i = 1:10
        [~, I_predicted] = max(R(:, i+1));
        clusterings(:,i) = cluster_assignment(I_predicted,:);
    end
    
    return
    
    [M_optimal,I_optimal] = max(R(:,1));
    [~, I_predicted] = max(R(:, end));
    real_RI = R(I_predicted, 1); % rand index obtained with the predicted WW
    
    learned_w = I_predicted - 1;
    
    disp('this is the learned_w')
    disp(learned_w)
    
    disp('this is the accompanying clustering')
    disp(cluster_assignment(I_predicted,:))
    
    disp('this is its RI, computed by me')
    disp(rand_index(cluster_assignment(I_predicted,:),label,'adjusted'))


%     disp(R);
    disp(strcat(['Optimal Rand-Index is ', num2str(M_optimal)]));
    disp(strcat(['Optimal warping window is ', num2str(I_optimal - 1)]));
    disp(strcat(['Adapted Rand-Index is ', num2str(real_RI)]));
    disp(strcat(['Adapted warping window is ', num2str(I_predicted - 1)]));
    disp("this is the R vector")
    disp(R)
    disp("\n\n and this is score 16 ")
    disp(score_16)
    disp(strcat(['RI at WW_0 is ', num2str(R(1,1))]));
    disp(strcat(['RI at WW_10 is ', num2str(R(11,1))]));
    disp('Displaying Rand-Index and prediction vectors');
    
% Plot result
%     R = zscore(R);
%     figure; 
%     plot(R(:, 1) + 10, 'r', 'LineWidth', 3); hold on;
%     plot(R(:, 2) + 5, 'b');
%     plot(R(:, 3), 'b');
%     plot(R(:, 4) - 5, 'b');
%     plot(R(:, 5) - 10, 'b');
%     plot(R(:, 6) - 15, 'b');
%     title('Learning w result');
%     box off;

    disp('FINISH');
    
end

%%
% Compute rand index
function RI=RandIndex(c1,c2)
% function RI=RandIndex(c1,c2)
%RANDINDEX - calculates Rand Indices to compare two partitions
% ARI=RANDINDEX(c1,c2), where c1,c2 are vectors listing the 
% class membership, returns the "Hubert & Arabie adjusted Rand index".
% [AR,RI,MI,HI]=RANDINDEX(c1,c2) returns the adjusted Rand index, 
% the unadjusted Rand index, "Mirkin's" index and "Hubert's" index.
%
% See L. Hubert and P. Arabie (1985) "Comparing Partitions" Journal of 
% Classification 2:193-218

%(C) David Corney (2000)   		D.Corney@cs.ucl.ac.uk

if nargin < 2 || min(size(c1)) > 1 || min(size(c2)) > 1
   error('RandIndex: Requires two vector arguments');
end

C=Contingency(c1,c2);	%form contingency matrix

n=sum(sum(C));
nis=sum(sum(C,2).^2);		%sum of squares of sums of rows
njs=sum(sum(C,1).^2);		%sum of squares of sums of columns

t1=nchoosek(n,2);	%total number of pairs of entities
t2=sum(sum(C.^2));	%sum over rows & columnns of nij^2
t3=.5*(nis+njs);

%Expected index (for adjustment)
nc=(n*(n^2+1)-(n+1)*nis-(n+1)*njs+2*(nis*njs)/n)/(2*(n-1));

A=t1+t2-t3;	%no. agreements
D=  -t2+t3;	%no. disagreements

if t1==nc
   AR=0;			%avoid division by zero; if k=1, define Rand = 0
else
   AR=(A-nc)/(t1-nc);		%adjusted Rand - Hubert & Arabie 1985
end

RI=A/t1;			%Rand 1971		%Probability of agreement
MI=D/t1;			%Mirkin 1970	%p(disagreement)
HI=(A-D)/t1;	%Hubert 1977	%p(agree)-p(disagree)

end

%%
function Cont=Contingency(Mem1,Mem2)

if nargin < 2 || min(size(Mem1)) > 1 || min(size(Mem2)) > 1
   error('Contingency: Requires two vector arguments');
end

Cont=zeros(max(Mem1),max(Mem2));

for i = 1:length(Mem1);
   Cont(Mem1(i),Mem2(i))=Cont(Mem1(i),Mem2(i))+1;
end
end

% Purpose: find the constraints (pairs of time series) that change label
% at least once. This reflects the effect of WW. We use TADpole here, but
% any other clustering algorithms can be used instead.
% Input: 
% data: a matrix with each row is a time series object
% label: column vector of cluster assignment(groundtruth)
% N: number of clusters (groundtruth, or use TADpole's recommendation)
% dc: TADpole cutoff distance 
% Output:
% volatile_constraints: list of constraints that is not always must-linked
% or cannot-linked. 
% cluster_result: a matrix of cluster assignment for each WW from 0-20. 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function[volatile_constraints, cluster_result] = filter_constraints(data, N, dc, max_W, train_idxs)
    n = size(data,2); %returns the second dimension of array data
    UBMatrix = single(findUBMatrix(data)); % the upper bound matrix
    number_of_instances = size(data,1);
    m = single(zeros(number_of_instances, number_of_instances));
    
    % a matrix to hold class assignment for each WW 0-20
    cluster_result = zeros(max_W + 1, number_of_instances);
    
    for index = 0:max_W
        disp(strcat(['Warping window width is ', num2str(index), ' %']));
        band = index/100;
        r = band;
        if (~exist('r','var')), r=0.1;       end 
        if (r<1),               r=ceil(n*r); end
        
        LBMatrix = single(findLBMatrix(data,r)); % the lower bound matrix
        [clAssignment,~] = TADPole(data,band,N,dc,UBMatrix,LBMatrix);
        clearvars LBMatrix;
        
        cluster_result(index + 1,:) = clAssignment;
        %for each WW, compute the full all pair matrix m with 1 being
        %clustered as linked and 0 as clustered as not-linked. Sum up these
        %matrice. 21 means the pair is always clustered as linked; 
        %0 means the pair is always clustered as not-linked.
        m = m + evaluate_clustering_result_local(clAssignment);
    end
    
    upper_m = triu(m, 1); % return the elements above the diagonal
    %finds constraints that are sometimes satisfied, sometimes not
    [row, col] = find(upper_m > 0 & upper_m < 21);
    v = single([row, col]); % list of volatile pairs
    
    disp(size(v))
    
    new_v = [];
    
    for v_idx = 1:size(v,1)
        if ~ismember(int64(v(v_idx,1)-1),train_idxs) | ~ismember(int64(v(v_idx,2)-1),train_idxs)
            continue
        end
        new_v = [new_v; v(v_idx,:)];
    end
    
    v = new_v;
    
    all_unique_constraints = (number_of_instances * (number_of_instances - 1))/2;
    right_wrong_constraints = size(v,1);
    percentage = 100*right_wrong_constraints/all_unique_constraints;
    disp(strcat('Total number of volatile constraints is :', num2str(right_wrong_constraints)));
    disp(strcat('Total number of all unique constraints is :', num2str(all_unique_constraints)));
    disp(strcat('That is :',num2str(percentage), '% of the number of unique constraints available'));
    
    %fill in result for linked, unlinked as WW changes
    %example: constraint 1-5: 0 0 1 1 1 ...1
    %compare against cluster result
    %cluster result is a matrix 21 x n, with n being the number of
    %instances
   
    %v_ is the temporary matrix to hold final result
    %number of rows is number of volatile constraints
    %there are 23 columns
    %the first two columns are the time series pair. e.g 2 3
    %the subsequent columns are linked (1) or not-linked (0) for WW 0-20 
    v_ = zeros(size(v,1), max_W + 3);
    for i = 1: size(v,1)
        pair = v(i,:);
        re = zeros(1,max_W+1);
        %check cluster result at each WW
        for k = 1:max_W+1
            cr = cluster_result(k,:);
            p1 = pair(:,1);
            p2 = pair(:,2);
            if cr(p1) == cr(p2)
                re(k) = 1;   %clustered as linked
            else
                re(k) = 0;
            end
        end
        % concatenate the pair with its cluster result for each WW
        v_(i,:) = [v(i,:), re]; 
    end
   volatile_constraints = v_;
   clearvars -except volatile_constraints cluster_result; 
end

%%
function [linked_or_not_constraints] = evaluate_clustering_result_local(class_assignment)
% find out if constraints are linked or unlinked
% Input:
% class_assignment: the clustering result
% label: the groundtruth
% Output:
% right_wrong_constraints: the result matrix of size n*n, with n being the
% number of instances in the data set.
% The value of each matrix element is 0 or 1
% 0 means unlinked
% 1 means linked
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
n = size(class_assignment,2);
M = zeros(n, n);

for i = 1: n
    for j = 1: n
        %check cluster assignment
        if class_assignment(i) == class_assignment(j)
            M(i, j) = 1; %linked
        elseif class_assignment(i) ~= class_assignment(j)
            M(i, j) = 0; %not-linked
        else
            M(i,j) = 0; %should never reach here as all cases are covered
        end
    end
end          
linked_or_not_constraints = M; % the matrix result with 1 and 0
end

% Purpose: compute simplicity score for each constraint and sort them in
% ascending order
% Input
% constraints: a matrix n * 23, in which n is the number of constraints
% for each row, the first two elements are the time series pair, 
% the next elements are 0 or 1; 0 means not-linked and 1 means linked.
% each row looks like this
% 2 3 0 0 0 1 1 1 1...1
% Output
% sorted_constraints: list of constraints sorted by their simplicity scores
% the lower score, the simpler
% it is matrix n * 24. The last column is the simplicity score.
%
%
%%
function [sorted_constraints] = simplicity_sort(constraints, max_W)
C_score = zeros(size(constraints,1), 1);
for i = 1:size(constraints,1)
    C = constraints(i,3:end);
    simplicity_score = 0;
    for k = 1:max_W
        if C(:,k) ~= C(:,k+1)
            simplicity_score = simplicity_score + 1;
        end
    end
    C_score(i) = simplicity_score;
end

cons = [constraints, C_score];

% sort matrix based on the the simplicity score, which is column 24
% clearvars -except cons;
sorted_constraints = sortrows(cons, max_W + 4);
clearvars cons;
end

% Purpose: To annotate the constraints and build the prediction vector based
% on how the constraints obey the groundtruth.
% Input:
% class_assignment: the class assignment vector from TADPole clustering
% label: the true class label; only a fraction of this will be used, based
% on which constraints are selected for annotation
% sorted_constraints: list of simplicity-sorted constraints
% number_of_constraints: how many constraints will be annotated. These can
% be linked or not-linked constraints. By definition the number of linked
% constraints is always much fewer than not-linked ones. Constraints are
% slected based on their simplicity score. The simpler (top) go first.
% Output:
% The score, which is the number of satisfied constraints over the number
% of all constraints attempted.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
function [final_score] = evaluate_constraint(class_assignment, label, sorted_constraints, number_of_constraints)

linked = [];
not_linked = [];
M = 1:number_of_constraints;
for i = 1:number_of_constraints
    pair = sorted_constraints(M(i), :);
    %get annotation
    if label(pair(1)) == label(pair(2))
        %disp('Groundth truth is must-link');
        linked = [linked; pair(1), pair(2)];
    else
        %disp('Groundtruth is cannot-link');
        not_linked = [not_linked; pair(1), pair(2)];
    end
end

%compares the clustering result with groundtruth
%increments the score by 1 for any constraints satisfied
score = 0;
%check the linked constraint
for i = 1:size(linked, 1)
   if class_assignment(linked(i,1)) == class_assignment(linked(i,2))
      score = score + 1;
   end
end

% check the not_linked constraints
for i = 1:size(not_linked,1)
   if class_assignment(not_linked(i,1)) ~= class_assignment(not_linked(i,2))
      score = score + 1; 
   end
end

%final score is ratio of constraints obeyed
final_score = score/number_of_constraints;

end

% The TADpole algorithm is a paper of Nurjahan Begum
% http://www.cs.ucr.edu/~nbegu001/SpeededClusteringDTW/
%
%%
function [clAssignment,distCalculationPercentage] = TADPole(data,band,N,dc,UBMatrix,LBMatrix)
    
    % correct asymmetricity of LB_Keogh
    LB = [];
    for i = 1:size(data,1)
        for j = 1:size(data,1)
            LB(i,j) = max(LBMatrix(i,j),LBMatrix(j,i));
        end
    end

    SparseFlagMatrix = zeros(size(data,1),size(data,1));
    SparseDistMatrix = zeros(size(data,1),size(data,1));

    %%%%% calculate Density%%%%
    RhoList = [];
    Rho = [];
    
    for i = 1:size(data,1) 
        list_ = [];
        for j = 1:size(data,1)
            if i == j
                continue; % PRUNE
            end
            if (LB(i,j) <= dc && UBMatrix(i,j) > dc)
                if SparseFlagMatrix(i,j)==0
                    SparseDistMatrix(i,j) = calculate_distance((data(i,:)),(data(j,:)),band); % DTW distance calculate
                    SparseFlagMatrix(i,j) = 1;
                    SparseDistMatrix(j,i) = SparseDistMatrix(i,j); 
                    SparseFlagMatrix(j,i) = 1;
                end

                if  SparseDistMatrix(i,j) <= dc
                    list_ = [list_ j];  
                end

            elseif LB(i,j) <= dc && UBMatrix(i,j) < dc % within dc, PRUNE
                SparseFlagMatrix(i,j) = 2;
                SparseFlagMatrix(j,i) = 2;
                 list_ = [list_ j];
            elseif LB(i,j) > dc % not within dc, PRUNE
                SparseFlagMatrix(i,j) = 3;
                SparseFlagMatrix(j,i) = 3;
            else % identical??
                SparseFlagMatrix(i,j) = 4;
                SparseFlagMatrix(j,i) = 4;
            end
        end
        RhoList{i} = list_;
        Rho = [Rho size(list_,2)];
    end

    Rho = (Rho-min(Rho))/(max(Rho)-min(Rho));

    %%%%% end calculate Density%%%%
    
    UpperTraingularFlagMatrix = triu(SparseFlagMatrix,0);
    [rhoSorted, sortIndices] = sort(Rho,'descend');
    
    %%%%% calculate upper bound to the NN distance from higher density list%%%%
   
    distCount = 0;
    DeltaList{1} = [];

    bsfGuess = [];
    bsfGuess = [bsfGuess inf];
    bsfSource = [];
    bsfSource = [bsfSource -1];

    for bsfInd_ = 2:size(data,1)
        currentItem = sortIndices(bsfInd_);
        DeltaList{bsfInd_} = intersect(find(Rho(:)>=Rho(sortIndices(bsfInd_)))',sortIndices(1:bsfInd_-1));%take greater equal density items except itself

        tempDel = DeltaList{bsfInd_};
        bsf = inf;
        bsfInd = -1;

        for j = 1:size(tempDel,2)
            i_ind = currentItem;
            j_ind = tempDel(1,j);

            if SparseFlagMatrix(i_ind,j_ind) == 1 || SparseFlagMatrix(j_ind,i_ind) == 1 % distance already computed
                if SparseDistMatrix(j_ind,i_ind) > SparseDistMatrix(i_ind,j_ind)
                    dist = SparseDistMatrix(j_ind,i_ind);
                else
                     dist = SparseDistMatrix(i_ind,j_ind);
                end
                if bsf > dist 
                    bsf = dist;
                    bsfInd = j_ind;
                end
            else
                dist = UBMatrix(i_ind,j_ind);
                if bsf > dist 
                    bsf = dist;
                    bsfInd = j_ind;
                end
            end
         end
        bsfGuess = [bsfGuess bsf];
        bsfSource = [bsfSource bsfInd];
    end
    %%%%% end calculate upper bound to the NN distance from higher density list%%%%
    
    [~,orderedIndices] = sort(rhoSorted.*bsfGuess,'descend'); % TADPole order
    
    %%%pruning algorithm%%%

    Delta = zeros(1,size(data,1));
    NNDelta = zeros(1,size(data,1));
    zVal = zeros(1,size(data,1));
    bsfActual = zeros(1,size(data,1));

    for i = 1:size(data,1)
        tempDel = DeltaList{orderedIndices(i)};
        tmpLB = [];

        for k = 1:size(tempDel,2)
            i_ind = sortIndices(orderedIndices(i));
            j_ind = tempDel(1,k);
            if LB(i_ind,j_ind) > bsfGuess(orderedIndices(i)) % PRUNING
                tmpLB = [tmpLB randi([9999,9999999],1)];
            else
                tmpLB = [tmpLB LB(i_ind,j_ind)];
            end
        end
         tempLB{i,1} = tmpLB;
         pruneIndices = find((tempLB{i,1}(:))>=9999);
         distIndices = setdiff([1:size(tmpLB,2)],pruneIndices);
         pruneCount = size(distIndices,2);

         itemsForDistanceCalculation = tempDel(distIndices); 

         DTWdists = [];
         DTWPoses = [];

         if size(itemsForDistanceCalculation,2) > 0
            for d = 1:size(itemsForDistanceCalculation,2)
                if SparseFlagMatrix(i_ind,itemsForDistanceCalculation(d)) == 1 || SparseFlagMatrix(itemsForDistanceCalculation(d),i_ind) == 1 % distance already computed
                    if SparseDistMatrix(i_ind,itemsForDistanceCalculation(d)) > SparseDistMatrix(itemsForDistanceCalculation(d),i_ind)
                        DTWdists = [DTWdists SparseDistMatrix(i_ind,itemsForDistanceCalculation(d))];
                        DTWPoses = [DTWPoses itemsForDistanceCalculation(d)];
                    else
                        DTWdists = [DTWdists SparseDistMatrix(itemsForDistanceCalculation(d),i_ind)];
                        DTWPoses = [DTWPoses itemsForDistanceCalculation(d)];
                    end
                else
                    newDist = calculate_distance((data(i_ind,:)),(data(itemsForDistanceCalculation(d),:)),band);
                    DTWdists = [DTWdists newDist];
                    DTWPoses = [DTWPoses itemsForDistanceCalculation(d)];

                    SparseFlagMatrix(i_ind,itemsForDistanceCalculation(d)) = 1;
                    SparseDistMatrix(i_ind,itemsForDistanceCalculation(d)) = DTWdists(end);
                    SparseFlagMatrix(itemsForDistanceCalculation(d),i_ind) = 1;
                    SparseDistMatrix(itemsForDistanceCalculation(d),i_ind) = DTWdists(end);

                    distCount = distCount + 1;
                end
            end
            [delVal, delInd] = min(DTWdists);
            Delta(sortIndices(orderedIndices(i))) = delVal;
            zVal(sortIndices(orderedIndices(i))) = delVal;
            bsfActual(sortIndices(orderedIndices(i))) = DTWPoses(delInd);
            NNDelta(sortIndices(orderedIndices(i))) = itemsForDistanceCalculation(delInd);
         else            
             Delta(sortIndices(orderedIndices(1))) = -1;
             NNDelta(sortIndices(orderedIndices(1))) = 0;
             zVal(sortIndices(orderedIndices(1))) = -1; 
             bsfActual(sortIndices(orderedIndices(1))) = 0; 
         end
    end
    
    b = sortIndices(orderedIndices(1:size(data,1)));
    a = sortIndices(orderedIndices);

    S = a(~ismembc(a(:), sort(b(:))));

    for re = 1:size(S,2)
        ind = find(sortIndices(:)==S(re));
        zVal(S(re)) = bsfGuess(ind);
        Delta(S(re)) = bsfGuess(ind);
        NNDelta(S(re)) = bsfSource(ind); 
    end

    maxDensityPoints = find(Delta(:)==-1);
    Delta(maxDensityPoints) = max(Delta);
    zVal(maxDensityPoints) = max(Delta);
    
    %%% end pruning algorithm%%% 
    distCalculated = distCount + (size(find(UpperTraingularFlagMatrix==1),1));
    distCalculationPercentage = (distCalculated/((size(data,1)*(size(data,1)+1))/2))*100;

    zVal = (zVal-min(zVal))/(max(zVal)-min(zVal));
    
    %%%% cluster assignment%%%%%
     clCenterGamma =  zVal.*Rho;

    [~,clCenterGammaSortedIndices] = sort(clCenterGamma,'descend');

    centers = clCenterGammaSortedIndices(2:N);
    centers = sort([centers clCenterGammaSortedIndices(1)]);

    clAssignment = ones(1,size(data,1))*(-1);
    
    for cen = 1:size(centers,2)
        clAssignment(centers(cen)) = cen;
    end
    NNs_ = NNDelta;
    
    for clA = 1:size(data,1)
        if clAssignment(sortIndices(clA))==-1
            clAssignment(sortIndices(clA)) = clAssignment(NNs_(sortIndices(clA)));
        end
    end
    %%%% end cluster assignment%%%%
end

% find upperbound of DTW
%%
function UBMatrix = findUBMatrix(data)
    UBMatrix = pdist(data);
    UBMatrix = squareform(UBMatrix);
end

% Find lowerbound of DTW
%%
function LBMatrix = findLBMatrix(data,r)
    LBMatrix = zeros(size(data,1),size(data,1));
    for i = 1:size(data,1)
        Q = data(i,:);
        U = []; L = [];
        for k = 1:size(Q,2)
            st = k - r;
            en = k + r;
            if st <0 || st == 0
                st = 1;
            end
            if en > size(Q,2)
                en = size(Q,2);
            end
            U = [U max(Q(st:en))];
            L = [L min(Q(st:en))];
        end
     
        for j = 1:size(data,1)
            C = data(j,:);
            for p = 1:size(C,2)
                if C(p) > U(p)
                   LBMatrix(i,j) = LBMatrix(i,j) + (C(p)-U(p)) .^2;
                elseif C(p) < L(p)
                   LBMatrix(i,j) = LBMatrix(i,j) + (C(p)-L(p)) .^2;
                else
                    LBMatrix(i,j) = LBMatrix(i,j) + 0;
                end
            end
            LBMatrix(i,j) = sqrt(LBMatrix(i,j));
        end
    end
end

% This is a wrapper function to use DTW_mex function, which is optimized
% for speed
% The window size is in range [0-1]
% When passped to function DTW_mex, window size is converted into the
% actual value of the Sakoe-chiba band, which is windowSize *
% timeSeriesLength
%%
function dist = calculate_distance(TimeSeries1,TimeSeries2,window_size)
    % compute DTW distance between two time series
% window_size is a fraction of time series length. Its values go from 0-1

r = window_size;
n = length(TimeSeries1);
m = length(TimeSeries2);

if (~exist('window_size','var'))
    disp('Setting window size to 0.1');
    r = ceil(min(n,m)*0.1);              
end
if (r<=1)
    r=ceil(min(n,m)*r);
else
    disp('Window size value must be 0 to 1');
    return;
end

% compute the DTW distance between two time series
% DTW_mex is C code compiled on Windows 64-bit machine
% r is how much the warping path can be deviated from the diagonal 
% r = windowSize * timeSesiesLength

d = DTW_mex(TimeSeries1,TimeSeries2,r);
dist = sqrt(d);

end





















