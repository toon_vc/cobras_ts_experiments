import numpy as np
import os
import json
import configparser
import sys
from cobras_ts import cobras_kshape

dataset = sys.argv[1]
fold = int(sys.argv[2])
budget = int(sys.argv[3])

config = configparser.ConfigParser()
config.read('../configs/main')

cur_path = os.path.dirname(os.path.abspath(__file__))
clusterings_path = os.path.join(config['main']['clusterings_path'],'cobras_kshape',dataset)
folds_path = config['main']['folds_path']
dists_path = os.path.join(config['main']['distance_matrices_path'],dataset)

data = np.loadtxt(os.path.join(config['main']['ucr_path'],dataset,dataset + '_TEST'), delimiter=',')
series = data[:,1:]
labels = data[:,0]

if not os.path.exists(os.path.join(clusterings_path)):
    os.makedirs(os.path.join(clusterings_path))

train_idx = np.load(os.path.join(folds_path,dataset,str(fold) + ".train.npy")).tolist()

clusterer = cobras_kshape.COBRAS_kShape(series, labels, budget, train_idx)
clusterings, runtimes, ml, cl = clusterer.cluster()

f = open(clusterings_path + "/" + "_".join(sys.argv[2:]), 'w+')
f.write(json.dumps({'clusterings' : clusterings, 'runtimes' : runtimes, 'ml' : ml, 'cl' : cl}))
f.close()
