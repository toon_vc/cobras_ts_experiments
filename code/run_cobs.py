import random
import time
import numpy as np
import os
import json
import configparser
import sys


def generate_pairs_in_fold(fold, k):

    generated_pairs = []

    while len(generated_pairs) < k:
        current_pair = random.sample(fold,2)
        element1 = min(current_pair)
        element2 = max(current_pair)

        if (element1,element2) in generated_pairs:
            continue

        generated_pairs.append((element1,element2))

    return generated_pairs


def select_clust_normal_reward(clust, descr, ml, cl):

    scores = np.zeros((len(clust),1))
    for ml_c in ml:
        for idx, c in enumerate(clust):
            if c[ml_c[0]] == c[ml_c[1]]:
                scores[idx] += 1

    for cl_c in cl:
        for idx, c in enumerate(clust):
            if c[cl_c[0]] != c[cl_c[1]]:
                scores[idx] += 1

    return clust[np.argmax(scores)], descr[np.argmax(scores)]


def get_best_pair_weighted(clust,descr,s_size,weights,pairs):

    n = len(clust[0])

    d = np.zeros((n,n))
    a = np.zeros((n,n))
    np.fill_diagonal(d,n)

    cur_best_value = len(clust)
    cur_best_c = None

    for cnt, (i,j) in enumerate(pairs):

        if cnt > s_size and cur_best_value < len(clust):
            break

        for c_cntr, c in enumerate(clust):
            if c[i] == c[j]:
                a[i,j] += weights[c_cntr]
            else:
                d[i,j] += weights[c_cntr]

        if abs(d[i,j] - a[i,j]) < cur_best_value:
            cur_best_value = abs(d[i,j] - a[i,j])
            cur_best_c = (i,j)

    return cur_best_c


def selection_weighted(clust, descr, true_clust, sample_size, training_set, exchange_factor, budget):
    combined = list(zip(clust, descr))
    random.shuffle(combined)
    clust[:], descr[:] = zip(*combined)

    pairs = generate_pairs_in_fold(training_set,sample_size)

    weights = np.ones((len(clust),1)) / len(clust)

    ml = []
    cl = []

    q_asked = 0

    winners = []
    winner_descriptions = []
    mls = []
    cls = []

    timings = []

    start = time.time()

    while True:
        if budget == 0:
            return winners, winner_descriptions, mls, cls, timings

        i, j = get_best_pair_weighted(clust, descr, sample_size, weights,pairs)

        pairs.remove((i,j))

        for c_cntr, (c,d) in enumerate(zip(clust,descr)):
            if c[i] == c[j] and true_clust[i] == true_clust[j]:
                weights[c_cntr] *= exchange_factor
            elif c[i] != c[j] and true_clust[i] != true_clust[j]:
                weights[c_cntr] *= exchange_factor
            else:
                weights[c_cntr] /= exchange_factor

        if true_clust[i] == true_clust[j]:
            ml.append((i,j))
        else:
            cl.append((i,j))

        q_asked += 1
        weights = weights / np.sum(weights)

        cur_winner, cur_winner_descr = select_clust_normal_reward(clust,descr,ml,cl)

        winners.append(cur_winner[:])
        winner_descriptions.append(cur_winner_descr)
        mls.append(ml[:])
        cls.append(cl[:])
        timings.append(time.time() - start)

        budget -= 1

    return winners, winner_descriptions, mls, cls, timings

dataset = sys.argv[1]
fold = int(sys.argv[2])
budget = int(sys.argv[3])
exchange_factor = float(sys.argv[4])
sample_size = int(sys.argv[5])
w = int(sys.argv[6])
alpha = float(sys.argv[7])

config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = os.path.join(config['main']['clusterings_path'],'cobs',dataset)
grid_path = os.path.join(config['main']['clusterings_path'],'cobs',dataset,'grid')
folds_path = config['main']['folds_path']
dists_path = os.path.join(config['main']['distance_matrices_path'],dataset)


if os.path.exists(clusterings_path + "/" + "_".join(sys.argv[2:])):
    exit()

data = np.loadtxt(os.path.join(config['main']['ucr_path'],dataset,dataset + '_TEST'), delimiter=',')
series = data[:,1:]
labels = data[:,0]

if not os.path.exists(os.path.join(clusterings_path)):
    os.makedirs(os.path.join(clusterings_path))

clust = []
descr = []
for fn in os.listdir(grid_path):
    f = open(os.path.join(grid_path,fn))
    run = json.load(f)
    f.close()
    clust.append(np.array(run["clustering"]))
    descr.append(fn)

train_idx = np.load(os.path.join(folds_path,dataset,str(fold) + ".train.npy")).tolist()


winners, winner_descriptions, mls, cls, timings = selection_weighted(clust, descr, labels, sample_size, train_idx, exchange_factor, 100)
winners = [x.tolist() for x in winners]

f = open(clusterings_path + "/" + "_".join(sys.argv[2:]), 'w+')
f.write(json.dumps({'clusterings' : winners, 'descriptions' : winner_descriptions, 'runtimes' : timings, 'ml' : mls, 'cl' : cls}))
f.close()
