import os
import numpy as np
import json
import traceback

import configparser
import sys

def rand_index(c1,c2):
    A = 0
    B = 0
    rest = 0

    for i in range(len(c1)):
        for j in range(i+1,len(c1)):
            if c1[i] == c1[j] and c2[i] == c2[j]:
                A += 1
            elif c1[i] != c1[j] and c2[i] != c2[j]:
                B += 1
            else:
                rest += 1

    return 1.0*(A + B) / (A + B + rest)




f = open('cdtwss_scores32')
lines = f.readlines()
f.close()

cssdtw_scores32 = dict()
for line in lines:
    els = line.split(',')
    cssdtw_scores32[els[0].replace(' ','')] = float(els[-1].strip())

config = configparser.ConfigParser()
config.read('../configs/main')



print("dataset,kShape,cDTWss with 32 constraints")

for dataset in ['Beef','MiddlePhalanxOutlineCorrect','ShapesAll','uWaveGestureLibrary_X','Ham','WordsSynonyms','Car','Worms','ECGFiveDays','InlineSkate','ToeSegmentation2','MiddlePhalanxTW','MedicalImages','FaceFour','ChlorineConcentration','Cricket_Y','FISH','MiddlePhalanxOutlineAgeGroup','Trace','DistalPhalanxOutlineAgeGroup','SonyAIBORobotSurfaceII','ProximalPhalanxOutlineAgeGroup','FacesUCR','Haptics','DiatomSizeReduction','Lighting7','InsectWingbeatSound','ECG5000','uWaveGestureLibrary_Y','Adiac','MoteStrain','Cricket_X','SonyAIBORobotSurface','Strawberry','SwedishLeaf','ScreenType','DistalPhalanxTW','ToeSegmentation1','Lighting2','ItalyPowerDemand','ProximalPhalanxTW','Herring','BirdChicken','CBF','TwoLeadECG','ECG200','WormsTwoClass','50words','CinC_ECG_torso','Computers','ProximalPhalanxOutlineCorrect','Symbols','Plane','yoga','synthetic_control','RefrigerationDevices','DistalPhalanxOutlineCorrect','PhalangesOutlinesCorrect','LargeKitchenAppliances','Gun_Point','FaceAll','uWaveGestureLibrary_Z','OSULeaf','ArrowHead']: 
    clusterings_path = os.path.join(config['main']['clusterings_path'],'kshape',dataset)
    scores_path = os.path.join(config['main']['scores_path'],'kshape',dataset)
    
    data = np.loadtxt(os.path.join(config['main']['ucr_path'], dataset, dataset + '_TEST'), delimiter=',')
    y_s = data[:, 0]

      
    kshape_ri = np.load(os.path.join(config['main']['scores_path'],'kshape',dataset,'ris.npy'))[0]
   
    k = dataset
    if dataset == 'Worms':
        k = 'Worm'
    if dataset == 'yoga':
        k = 'Yoga'
    if dataset == 'uWaveGestureLibrary_Z':
        k = 'uWaveGestureLibrary_z'
    if dataset == 'uWaveGestureLibrary_X':
        k = 'uWaveGestureLibrary_x'
    if dataset == 'uWaveGestureLibrary_Y':
        k = 'uWaveGestureLibrary_y'
    if dataset == 'WordsSynonyms':
        k = 'WordSynonyms'
    if dataset == 'FISH':
        k = 'Fish'
    if dataset == 'FacesUCR':
        k = 'Faces_UCR'
    if dataset == 'WormsTwoClass':
        k = 'WormTwoClass'
    if dataset == 'Symbols':
        k = 'Symbol'
    if dataset == 'synthetic_control':
        k = 'Synthetic_Control'

    print(dataset + "," + "{0:.4f}".format(kshape_ri) + "," + str(cssdtw_scores32[k]))

