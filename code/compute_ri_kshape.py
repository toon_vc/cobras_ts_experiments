import os
import sys
import numpy as np
import json
import configparser


def rand_index(c1,c2):
    A = 0
    B = 0
    rest = 0

    for i in range(len(c1)):
        for j in range(i+1,len(c1)):
            if c1[i] == c1[j] and c2[i] == c2[j]:
                A += 1
            elif c1[i] != c1[j] and c2[i] != c2[j]:
                B += 1
            else:
                rest += 1

    return 1.0*(A + B) / (A + B + rest)

dataset = sys.argv[1]

budget = 100

config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = os.path.join(config['main']['clusterings_path'],'kshape',dataset)
scores_path = os.path.join(config['main']['scores_path'],'kshape',dataset)

if not os.path.exists(scores_path):
    os.makedirs(scores_path)

data = np.loadtxt(os.path.join(config['main']['ucr_path'], dataset, dataset + '_TEST'), delimiter=',')
labels = data[:, 0]

with open(clusterings_path) as result_file:
    result_data = json.load(result_file)
    ri = rand_index(result_data['clustering'], labels)
    np.save(scores_path + "/ris", np.array([ri]))
