function generate_clusterings(datasets,fold,folds_path,ucr_path,result_path)

keySet = {'50words','Adiac','Beef','CBF','ChlorineConcentration','CinC_ECG_torso','Coffee','Cricket_X','Cricket_Y','Cricket_Z','DiatomSizeReduction','ECG200','ECGFiveDays','FaceAll','FaceFour','FacesUCR','FISH','Gun_Point','Haptics','InlineSkate','ItalyPowerDemand','Lighting2','Lighting7','MALLAT','MedicalImages','MoteStrain','NonInvasiveFatalECG_Thorax1','NonInvasiveFatalECG_Thorax2','OliveOil','OSULeaf','SonyAIBORobotSurface','SonyAIBORobotSurfaceII','SwedishLeaf','Symbols','synthetic_control','Trace','Two_Patterns','TwoLeadECG','uWaveGestureLibrary_X','uWaveGestureLibrary_Y','uWaveGestureLibrary_Z','WordsSynonyms','yoga','Car','Plane','ArrowHead','BeetleFly','BirdChicken','Computers','DistalPhalanxOutlineAgeGroup','DistalPhalanxOutlineCorrect','DistalPhalanxTW','ECG5000','Ham','HandOutlines','Herring','InsectWingbeatSound','LargeKitchenAppliances','Meat','MiddlePhalanxOutlineAgeGroup','MiddlePhalanxOutlineCorrect','MiddlePhalanxTW','PhalangesOutlinesCorrect','Phoneme','ProximalPhalanxOutlineAgeGroup','ProximalPhalanxOutlineCorrect','ProximalPhalanxTW','RefrigerationDevices','ScreenType','ShapesAll','SmallKitchenAppliances','Strawberry','ToeSegmentation1','ToeSegmentation2','UWaveGestureLibraryAll','Worms','WormsTwoClass','StarLightCurves','ElectricDevices','Wine','wafer','FordA','FordB','ShapeletSim','Earthquakes','Cricket_X','Cricket_Y'}
valueSet = [3.0,3.0,3.0,4.4316,3.0,3.0,10.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,7.0,3.0,3.0,3.0,5.0,3.0,3.0,15.0,15.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,1.8619,3.0,3.0,3.0,3.8619,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,3.0,20.0,20.0,10.0,3.0,3.0,3.0,3.0,5.0,3.0,3.0,3.0,10.0,3.0,1.0,3.0,1.0,3.0,20.0,1.0,1.0,1.0,20.0,10.0,3.0,3.0,3.0,10.0,10.0,10.0,20.0,20.0,3.0,3.0,1.0,3.0,3.0,30.0,100.0,30.0,20.0,20.0]

mapObj = containers.Map(keySet,valueSet)

for str =datasets
    dataset = str{1}
    train_idxs = readNPY(strcat(folds_path,dataset,'/',fold,'.train.npy'));
%    try
        data = csvread(strcat(ucr_path,dataset,'/',dataset,'_TEST'));
	   tic
        disp('running with cutoff')
        mapObj(dataset)
        cluster_assignment = main_clustering(data, mapObj(dataset), 20,train_idxs);
        t = toc;
        csvwrite(strcat(result_path,dataset,'.',fold,'.runtime'),t);
        csvwrite(strcat(result_path,dataset,'.',fold), cluster_assignment);
 %   catch
 %       disp('failed for dataset')
 %       disp(dataset)
 %   end
end


end
