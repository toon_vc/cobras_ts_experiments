import os
import sys
import numpy as np
import json
import configparser
from sklearn import metrics

dataset = sys.argv[1]
fold = int(sys.argv[2])
budget = int(sys.argv[3])
exchange_factor = float(sys.argv[4])
sample_size = int(sys.argv[5])
w = int(sys.argv[6])
alpha = float(sys.argv[7])

config = configparser.ConfigParser()
config.read('../configs/main')

clusterings_path = os.path.join(config['main']['clusterings_path'],'cobs',dataset)
scores_path = os.path.join(config['main']['scores_path'],'cobs',dataset)
folds_path = os.path.join(config['main']['folds_path'],dataset)

if not os.path.exists(scores_path):
    os.makedirs(scores_path)

test_idx = np.load(os.path.join(folds_path, str(fold) + ".test.npy")).tolist()

data = np.loadtxt(os.path.join(config['main']['ucr_path'], dataset, dataset + '_TEST'), delimiter=',')
labels = data[:, 0]

labels_to_evaluate = [x for i, x in enumerate(labels) if i in test_idx]

with open(clusterings_path + "/" + "_".join(sys.argv[2:])) as result_file:
    result_data = json.load(result_file)

    aris = np.zeros((budget,))
    for idx, c in enumerate(result_data['clusterings']):
        to_evaluate = [x for i, x in enumerate(c) if i in test_idx]
        aris[idx] = metrics.adjusted_rand_score(labels_to_evaluate, to_evaluate)

    np.save(scores_path + "/" + "_".join(sys.argv[2:]) + ".aris", aris)